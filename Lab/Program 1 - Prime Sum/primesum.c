#include <stdio.h>
#include <stdlib.h>

#define LIMIT 10000

int *calculatePrimeNumbers();

void findPrimeNumbers(int number, int primes[]);

int sumArray(int primesToSum[], int length);

int main(int argc, char *argv[]) {
    // Determines the number of times a prime number will be calculated
    int numCases;
    scanf("%d", &numCases);

    // Build a list of prime numbers up to the defined limit
    int *primes;
    primes = calculatePrimeNumbers();

    int i;
    for (i = 0; i < numCases; i++) {
        // Which P(n) to calculate
        int current;
        scanf("%d", &current);

        // Find and calculate the prime numbers
        findPrimeNumbers(current, primes);
    }

    // Free the allocated space for prime numbers
    free(primes);
    return 0;
}

/**
 * Determine which numbers are prime
 *
 * @returns     An integer array indicating which values are considered prime
 */
int *calculatePrimeNumbers() {
    int *primes;
    primes = malloc(sizeof(int) * LIMIT);

    int i, j;
    for (i = 2; i < LIMIT; i++) {
        primes[i] = 1;
    }

    for (i = 2; i < LIMIT; i++) {
        if (primes[i]) {
            for (j = i; i * j < LIMIT; j++) {
                primes[i * j] = 0;
            }
        }
    }

    return primes;
}

/**
 * Finds the amount of prime numbers as indicated by the P(n) test case
 *
 * @param number    The amount of prime numbers to sum
 * @param primes    An array containing all prime numbers up to the defined limit
 */
void findPrimeNumbers(int number, int primes[]) {
    // Allocate memory for an array to store all prime numbers to sum
    int *primesToSum = malloc(sizeof(int) * number);

    int i;
    int z = 0;
    for (i = 0; i < LIMIT; i++) {
        // If the z accumulator is greater than the number of primes to sum,
        // break the loop
        if (z == number) {
            break;
        }

        // If the current index matches a prime number, add it to the primesToSum array
        if (primes[i]) {
            primesToSum[z] = i;
            z++;
        }
    }

    // Calculate sum of primes and print result
    int result = sumArray(primesToSum, number);
    printf("%d\n", result);

    // Free allocated memory
    free(primesToSum);
}

/**
 * Sums all of the prime numbers in an array
 *
 * @param primesToSum   An integer array containing prime numbers
 * @param length        An integer that indicates the length of the prime numbers array
 * @returns             An integer containing the sum of all prime numbers in the array
 */
int sumArray(int primesToSum[], int length) {
    int i, sum = 0;
    for (i = 0; i < length; i++) {
        sum = sum + primesToSum[i];
    }
    return sum;
}
