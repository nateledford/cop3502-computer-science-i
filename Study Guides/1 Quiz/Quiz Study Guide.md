Quiz Study Guide
================

Note: Please answer all coding questions with valid C code. You may assume for any question
that the appropriate includes have been made and that `i`, `j` and `k` are declared and ready to use as
integer variables (as loop indexes).

For each of the following questions, you may assume that both `size1` and `size2` are integer
variables that have been assigned to values prior to the line of code you are to write.

1. Write a single line of code that dynamically allocates an array of `size1` variables of type double.  
Assign the memory to a pointer called `prices`.

    ```c++
    double* prices = malloc(sizeof(double) * size1);
    ```

2. Write a single line of code that frees the memory allocated in question 1.

    ```c++
    free(prices);
    ```

3. Write a segment of code that allocates a two dimensional array of characters with dimensions `size1` by `size2`. 
The array should be prepared to hold `size1` strings of length `size2-1` or less. 
Assign the memory allocated to a pointer called `words`.

    ```c++
    char** words = (char**)malloc(sizeof(char*) * size1);
    for (i = 0; i < size1; i++)
        words[i] = (char*)malloc(sizeof(char) * size2-1);
    ```
    
4. Write a segment of code that frees the memory allocated in question 3.
    
    ```c++
    for(i = 0; i < size1; i++)
        free(words[i];
    free(words);
    ```

5.  This question involves the following struct:
    
    ```c++
    struct student {
        char* name;
        int ID;
        double balance;
    };
    ```
    
    Assuming that all pointers are stored using 8 bytes, integers are stored using 4 bytes, and doubles
are stored using 8 bytes, what is the value of the expression `sizeof(struct student)`?

    Answer: `20`
    
6. Write a segment of code that allocates an array of pointers to `struct student` of length
`size1`, then allocates each of the `struct student` variables, followed by allocating the name
pointer in each of the structs to a character array of length `size2`. (Note: there are multiple
possible orders for allocating the necessary memory and you may answer with any of these.)
    
    ```c++
    struct student** myclass = malloc(sizeof(struct student*) * size1);
    for (i = 0; i < size1; i++)
        students[i] = malloc(sizeof(struct student))
        students[i]-> name = malloc(sizeof(char) * size2);
    ```
    
7. Write a segment of code that frees all of the memory allocated in question 6.

    ```c++
    for(i = 0; i < size1; i++)
        free(students[i]->name);
        free(students[i]);
    free(students);
    ```
    
_____________

Binary Search

```c++
int binarysearch(int searchval, int* array, int length) {
    int low = 0;
    int high = length - 1;
    
    while (low <= high) {
        int mid = (low + high)/2;
        
        // Search value is too small
        if (searchval < array[mid])
            high = mid - 1;
        // Search value is too big
        else if (searchval > array[mid])
            low = mid + 1;
        else
            // Found it
            return 1;
    }
    
    // Did not find it
    return 0;
}
```
    
__________

Sorted List

```c++
while (i < size1 && j < size2) {
    // Match found. Move both pointers up list
    if (list1[i] == list2[j])
        i++;
        j++;
    // Item in first list is too small, move it's pointer up
    else if (list1[i] < list2[j])
        i++;
    // Item in second list is too small, move it's pointer up
    else
        j++;
}
```
