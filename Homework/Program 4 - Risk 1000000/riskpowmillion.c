/*
 * Nathaniel Ledford
 * riskpowmillion.c
 * 3/1/2016 - 3/6/2016
 */

#include <stdlib.h>
#include <stdio.h>

// Prototypes
void getArmies(int *army, int size);

void quickSort(int *army, int left, int right, int size);

int partition(int *army, int left, int right, int pivot);

void swap(int *x, int *y);

int determine_defeats(int *attackingArmy, int *defendingArmy, int size);

int main(int argc, char *argv[]) {
    // Read number of test cases
    int numCases;
    scanf("%d", &numCases);

    // i - loops through test cases
    int i;
    for (i = 0; i < numCases; i++) {
        // Read number of armies (each side)
        int numArmies;
        scanf("%d", &numArmies);

        // Dynamically allocate space for each army
        int *attackingArmies = malloc(numArmies * sizeof(int));
        int *defendingArmies = malloc(numArmies * sizeof(int));

        // Read in values for attacking/defending armies
        getArmies(attackingArmies, numArmies);
        getArmies(defendingArmies, numArmies);

        // Sort armies via quick sort
        quickSort(attackingArmies, 0, numArmies - 1, numArmies);
        quickSort(defendingArmies, 0, numArmies - 1, numArmies);

        // Tally number of defeats and print out result
        int defeats;
        defeats = determine_defeats(attackingArmies, defendingArmies, numArmies);
        printf("%d", defeats);

        // Free dynamically allocated space for each army
        free(attackingArmies);
        free(defendingArmies);

        printf("\n");
    }

    return 0;
}

/**
 * Reads into army values from stdin into an army array
 *
 * @param army      A pointer to an army array
 * @param size      The size of the army array
 */
void getArmies(int *army, int size) {
    int i;
    for (i = 0; i < size; i++) {
        scanf("%d", &army[i]);
    }
}

/**
 * Performs a recursive quick sort on a given array
 *
 * @param army      A pointer to an army array
 * @param left      The leftmost index of the army array
 * @param right     The rightmost index of the army array
 * @param size      The size of the army array
 */
void quickSort(int *army, int left, int right, int size) {
    // Add variables for pivot value and index
    int pivot;
    int pIndex;

    // Base case: if the array boundaries are not equal
    if (left < right) {
        // Determine the index of the pivot
        pIndex = (left + right) / 2;

        // Obtain the pivot value
        pivot = partition(army, left, right, pIndex);

        // Recursively sort each array subsection
        quickSort(army, left, pivot - 1, size);
        quickSort(army, pivot + 1, right, size);
    }
}

/**
 * Divides an array at the given pivot index
 *
 * @param army      A pointer to the current army array
 * @param left      The leftmost index of the current array subsection
 * @param right     The rightmost index of the current array subsection
 * @param pivot     The index of the pivot in the current array subsection
 */
int partition(int *army, int left, int right, int pivot) {
    int i;
    int pValue = army[pivot];

    // Start index at current leftmost (first) value in army array
    int index = left;

    // Perform first swap
    swap(&army[pivot], &army[right]);

    // Loop through values from left to right
    for (i = left; i < right; i++) {
        // If the current index value is less than the pivot value, perform a swap
        if (army[i] < pValue) {
            swap(&army[i], &army[index]);
            index += 1;
        }
    }
    swap(&army[index], &army[right]);
    return index;
}

/**
 * Swaps two integer values in array via pointers.  Uses a temporary variable.
 *
 * @param x     A value to be swapped
 * @param y     A value to be swapped
 */
void swap(int *x, int *y) {
    int temp = *x;
    *x = *y;
    *y = temp;
}

/**
 * Determines the number of times a defending army can defeat the attacking army
 *
 * @param attackingArmy     The attacking army (a/k/a the army you wish to defeat)
 * @param defendingArmy     The defending army that attempts to defeat the attacking army
 * @param size              The size of each army's arrays
 */
int determine_defeats(int *attackingArmy, int *defendingArmy, int size) {
    // Initialize the total number of defeats to zero
    int numDefeats = 0;

    // Loop through each army to determine the number of defeats
    // i - Loops through the defending army, j - loops through the attacking army
    int i = 0, j = 0;
    while (j < size && i < size) {
        // If the current defending army can defeat the attacking army
        // (If the current index of the defending army is greater than the index of the attacking army)
        if (attackingArmy[j] < defendingArmy[i]) {
            // Increment the number of defeats and both loop counters
            numDefeats++;
            j++;
            i++;
        }
        else {
            // Move on to next defending army
            i++;
        }
    }
    return numDefeats;
}

