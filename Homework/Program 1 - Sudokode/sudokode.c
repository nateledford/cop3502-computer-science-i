#include <stdio.h>

#define SIZE 9

int checkSet(int set[SIZE]);

int checkBoxes(int board[][SIZE], int start_row, int start_col);

void checkSolution(int board[][SIZE]);

/**
 * Solves a Sudoku puzzle based on given input
 */
int main(int argc, char **argv) {
    // Read number of puzzles to be solved
    int numPuzzles;
    scanf("%d", &numPuzzles);

    // Each puzzle is a 9 x 9 grid
    int board[SIZE][SIZE];

    // Loop through each puzzle
    for (int i = 0; i < numPuzzles; i++) {
        // Loop through rows
        for (int j = 0; j < SIZE; j++) {
            // Read current line of input and store in char array
            char line[SIZE + 1];
            scanf("%s", line);

            // Loop through columns
            for (int k = 0; k < SIZE; k++) {
                // Add current value from char array to puzzle board
                board[j][k] = line[k] - '0';
            }
        }

        // Determine whether or not the current solution is valid
        checkSolution(board);
    }
    return 0;
}

/**
* Determines if the provided solution to a sudoku puzzle is valid
* 
* @param board  2D array containing the current sudoku solution to be checked
*/
void checkSolution(int board[][SIZE]) {
    // Holds the result of each function determining the validity of the provided solution
    int is_valid;

    // Check Rows
    for (int i = 0; i < SIZE; i++) // Row
    {
        // Determine if the current row is valid
        is_valid = checkSet(board[i]);
        if (is_valid == 0) {
            printf("NO\n");
            return;
        }

        // Check columns
        int col[SIZE];
        for (int j = 0; j < SIZE; j++) {
            // Copy values for current column from board to an column array
            col[j] = board[i][j];
        }

        // Determine if current column is valid
        is_valid = checkSet(col);
        if (is_valid == 0) {
            printf("NO\n");
            return;
        }
    }

    // Check each 3 x 3 section of board
    // Start looping through rows...
    for (int i = 0; i < SIZE; i += 3) {
        // ...and columns
        for (int j = 0; j < SIZE; j += 3) {
            // Determine if current 3x3 section is valid
            is_valid = checkBoxes(board, i, j);
            if (is_valid == 0) {
                printf("NO\n");
                return;
            }
        }
    }

    printf("YES\n");
}

/**
 * Checks a set of numbers for duplicate values via brute force
 * If a duplicate value is found, break loops and return 1
 * Else, return zero if no duplicates are found
 *
 * @param set   The current set of numbers to check
 */
int checkSet(int set[SIZE]) {
    for (int i = 0; i < SIZE; i++) {
        for (int j = i + 1; j < SIZE; j++) {
            if (set[i] == set[j]) {
                return 0;
            }
        }
    }

    return 1;
}

/**
 * Checks both columns and rows of a 3x3 section in on the sudoku board
 * If the box does not contain any duplicate values, the 3x3 section
 * is valid
 *
 * @param board     2D array containing the sudoku solution to be checked
 * @param start_row     Integer denoting which row from where to start validation
 * @param start_col     Integer denoting which column from where to start validation
 */
int checkBoxes(int board[][SIZE], int start_row, int start_col) {
    // Store values from current section in array
    int box[SIZE];
    
    // Used to build the set of numbers from current 3x3 box
    int index = 0;
    
    // Loop through the current 3x3 box in the puzzle
    for (int r = start_row; r < start_row + 3; r++) {
        for (int c = start_col; c < start_col + 3; c++) {
            // Add numbers from box to set to validate
            box[index++] = board[r][c];
        }
    }
    return checkSet(box);
}
