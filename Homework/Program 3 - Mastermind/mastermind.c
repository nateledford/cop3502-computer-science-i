/*
 * Nathaniel Ledford
 * mastermind.c
 * 2/20/2016
 */

// Program will run, but does not replicate expected results based on provided test cases

#include <stdio.h>

#define MAXMOVES 20
#define MAXSLOTS 1000000

struct Guess buildGuess(int slots);

void makeGuess(int size, int count, int *guess, int *test, int black, int white);

int testPegs(int *guess, int *test, int black, int white, int size);

int countBlackPegs(int *guess, int *test, int length);

void getColorFrequency(int *combo, int *frequency, int length);

// Used for storing data about a single guess
struct Guess {
    int guess[MAXSLOTS];
    int blacks;
    int whites;
};

// Stores the total possible combinations for a game
int combinations = 0;

// Stores all of the guesses made in a game
struct Guess guesses[MAXMOVES];

int c; // Number of input cases
int n; // Number of slots
int k; // Number of possible colors
int m; // Number of moves that have been played

int main(int argc, char *argv[]) {
    // Read input cases
    scanf("%d", &c);

    // i - input cases, j - number of moves
    int i, j;
    for (i = 0; i < c; i++) {
        // Reset current count of black and white pegs

        combinations = 0;

        // Read in slots, colors, and number of moves completed
        scanf("%d %d %d", &n, &k, &m);

        // Create test array
        int test[n];
        for (j = 0; j < n; j++) {
            test[j] = 0;
        }

        // Loop through completed moves
        for (j = 0; j < m; j++) {
            // Build guess struct from existing moving data
            struct Guess g = buildGuess(n);
            guesses[j] = g;

            // Make a guess based on existing moves
            makeGuess(n, j, guesses[j].guess, test, guesses[j].blacks, guesses[j].whites);
        }
        // Print out the result
        printf("%d\n", combinations);
    }

    return 0;
}

/*
 * Builds a guess structure based on inputted guess data
 *
 * @param slots     The total number of slots being used in the current game
 */
struct Guess buildGuess(int slots) {
    // Create blank struct
    struct Guess g;

    // i - number of slots
    int i;
    for (i = 0; i < slots; i++) {
        // Read in current move into struct
        scanf("%d", &g.guess[i]);
    }

    // Read the number of black and white pegs
    scanf("%d", &g.blacks);
    scanf("%d", &g.whites);

    return g;
}

/*
 * Makes a guess based on a provided move and test.
 * Calls itself recursively if not all guesses have been made.
 *
 * @param slots     The total number of slots being used in the current game
 * @param count     The number of guesses made in the current move
 * @param guess     An array pointer to the current guess
 * @param test      An array pointer to the current test
 * @param black     The number of black pegs
 * @param white     The number of white pegs
 */
void makeGuess(int slots, int count, int *guess, int *test, int black, int white) {
    if (count == slots) {
        int rightGuess = testPegs(guess, test, black, white, slots);
        if (rightGuess == 1) {
            combinations += 1;
        }
        return;
    } else {
        int i;
        for (i = 0; i < k; i++) {
            test[count] = i;
            makeGuess(slots, count + 1, guess, test, black, white);
        }
    }
}

/*
 * Tests the current guess against the current test
 *
 * @param guess     An array pointer to the current guess
 * @param test      An array pointer to the current test
 * @param black     The number of black pegs for the guess
 * @param white     The number of white pegs for the guess
 * @param slots     The total number of slots being used in the current game
 */
int testPegs(int *guess, int *test, int black, int white, int slots) {
    // i - number of slots, j - counting black and white pegs
    int i, j;

    // New black and white peg counts
    int b = 0;
    int w = 0;

    // Keep track of correct guesses
    int guess_hit[slots];
    int test_hit[slots];

    // Set array values to zero
    for (i = 0; i < slots; i++) {
        guess_hit[i] = 0;
        test_hit[i] = 0;
    }

    // Loop through current guess and test
    for (i = 0; i < slots; i++) {
        // If an exact match is found, increment one black peg
        if (guess[i] == test[i]) {
            b += 1;
            guess_hit[i] = 1;
            test_hit[i] = 1;
        }
        if (b > black) {
            break;
        }
    }

    // If the black pegs for guess and test equal each other AND
    // the number of black pegs does not equal the number of available slots
    // and the number of white pegs is zero
    if (b == black && !(b == slots && white == 0)) {
        // Loop through the number of slots
        for (i = 0; i < slots; i++) {
            // Only loop through a guess index if it does not already have a black peg
            for (j = 0; j < slots && test_hit[i] == 0; j++) {
                // If there is an indirect match, increment white pegs
                if (guess[i] == test[j] && guess_hit[j] == 0) {
                    w += 1;
                    test_hit[i] = 1;
                    guess_hit[i] = 1;
                }
            }
            if (w > white) {
                break;
            }
        }
    }

    if (b != black || w != white) {
        return 0;
    } else {
        return 1;
    }
}

/**
 * Determines the number of black pegs based on a guess and a correct secret code
 *
 * @param guess     Pointer to a guess integer array
 * @param test      Pointer to a test integer array (The correct secret code)
 * @param length    The number of available slots
 * @returns         The number of black pegs
 */
int countBlackPegs(int *guess, int *test, int length) {
    int blackPegs = 0;

    // Loop through both arrays and check for exact matches (black pegs)
    int i;
    for (i = 0; i < length; i++) {
        if (guess[i] == test[i]) {
            blackPegs += 1;
        }
    }

    return blackPegs;
}

/*
 * Determines the frequency of colors in a given combination
 *
 * @param combo         Pointer to a combination of colors (a guess)
 * @param frequency     Pointer to the current frequency array
 * @param length        The number of available slots
 */
void getColorFrequency(int *combo, int *frequency, int length) {
    int i, j;
    for (i = 0; i < length; i++) {
        for (j = 0; j < k; j++) {
            if (combo[i] == j) {
                frequency[j] += 1;
            }
        }
    }
}

