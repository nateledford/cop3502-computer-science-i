#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define WORDLENGTH 20

const int DX_SIZE = 8;
const int DX[] = {-1, -1, -1, 0, 0, 1, 1, 1};
const int DY[] = {-1, 0, 1, -1, 1, -1, 0, 1};


char **loadDictionary(int *wordsCount);

void freeDictionary(char **dictionary, int *numWords);

char **buildGrid(int rows, int cols);

void freeGrid(char **grid, int rows);

int inBounds(int x, int y, int i, int j);

void searchGrid(int gridCase);

void findWords(char **grid, int i, int j, int x, int y);

int binsearch(int low, int high, char *searchVal, char **dictionary);

int numWords;
char **dictionary;

int main(int argc, char *argv[]) {
    int loop, numCases;

    // Read dictionary into application
    dictionary = loadDictionary(&numWords);

    // Read the number of word search puzzles to solve
    scanf("%d", &numCases);

    // Solve each puzzle
    for (loop = 0; loop < numCases; loop++) {
        searchGrid(loop);
    }

    // Free the memory allocated for the dictionary
    freeDictionary(dictionary, &numWords);
    return 0;
}

/**
 * Loads a dictionary from a binary text file into system memory.
 * Memory for the dictionary and each word is dynamcially allocated
 *
 * @param wordsCount    A pointer that keeps track of the total number of words in the dictionary
 * @return              An array of char arrays containing each word in the dictionary
 */
char **loadDictionary(int *wordsCount) {
    int i;

    // Open dictionary
    FILE *fp = fopen("dictionary.txt", "r");

    // Display an error message if the dictionary fails to open
    if (fp == 0) {
        fprintf(stderr, "Failed to open dictionary.txt\n");
        exit(1);
    }

    // Read the number of words stored in the dictionary
    fscanf(fp, "%d", wordsCount);

    // Dynamically allocate space for the dictionary
    char **dictionary = (char **) malloc(*wordsCount * sizeof(char *));

    // Store each word from the dictionary in memory
    for (i = 0; i < *wordsCount; i++) {
        dictionary[i] = malloc(WORDLENGTH * sizeof(char));

        fscanf(fp, "%s", dictionary[i]);
    }

    // Close the binary text file
    fclose(fp);

    return dictionary;
}

/**
 * Frees the dynamically allocated space used by the dictionary
 *
 * @param dictionary    The array to be freed
 * @param numWords      The number of words in the dictionary; used to free each char array
 */
void freeDictionary(char **dictionary, int *numWords) {
    int i;
    for (i = 0; i < *numWords; i++) {
        free(dictionary[i]);
    }
    free(dictionary);
}

/**
 * Constructs the puzzle grid used for the word search
 *
 * @param rows      The number of rows to be in the grid
 * @param cols      The number of cols to be in the grid
 * @return          A populated word search grid
 */
char **buildGrid(int rows, int cols) {
    // Counters used for rows and columns
    int i, j;

    // Dynamically allocate space the grid
    char **grid = malloc(rows * sizeof(char *));
    char line[rows + 1];

    // Dynamically allocate space for each line in the grid
    for (i = 0; i < rows; i++) {
        grid[i] = malloc((cols + 1) * sizeof(char));
    }

    // Loop through rows and cols and populate the grid with letters
    for (i = 0; i < rows; i++) {
        scanf("%s", line);
        for (j = 0; j < cols; j++) {
            grid[i][j] = line[j];
        }
    }

    return grid;
}

/**
 * Frees the dynamically allocated space used by the word search grid
 *
 * @param grid  The populated word search grid
 * @param rows  The number of rows in the grid
 */
void freeGrid(char **grid, int rows) {
    int i;
    for (i = 0; i < rows; i++) {
        free(grid[i]);
    }
    free(grid);
}


/**
 * Check if the location is inside the board boundaries
 *
 * @param nextX     The next row position in the grid
 * @param nextY     The next column position in the grid
 * @param x         The current row position in the grid
 * @param y         The current column position in the grid
 * @return          1 (true) if the current position is in bounds, otherwise returns 0 (false)
 */
int inBounds(int nextX, int nextY, int x, int y) {
    if (nextX > -1 && nextY > -1 && nextX < x && nextY < y) {
        return 1;
    }

    return 0;
}

/**
 * Initializes a search for words in the current word search grid
 *
 * @param gridCase  The current word search grid to be solved
 */
void searchGrid(int gridCase) {
    // Counters used in for loops
    int i, j;

    // Rows and columns
    int x, y;
    printf("Words Found Grid #%d:\n", gridCase + 1);

    // Read the number of rows and columns in the current grid and build the grid
    scanf("%d %d", &x, &y);
    char **grid = buildGrid(x, y);

    // Search for words in each row and column
    for (i = 0; i < x; i++) {
        for (j = 0; j < y; j++) {
            findWords(grid, i, j, x, y);
        }
    }

    // Free the space allocated for the current grid
    freeGrid(grid, x);
}

/**
 * Searchs for words in the current grid
 *
 * @param grid  The current word search grid
 * @param i     The current row in the word search grid
 * @param j     The current column in the word search grid
 * @param x     The total number of rows in the word search grid
 * @param y     The total number of columns in the word search grid
 */
void findWords(char **grid, int i, int j, int x, int y) {
    // Loop through all possible directions for the current grid position
    for (int k = 0; k < DX_SIZE; k++) {
        int currentX = i;
        int currentY = j;

        // Candidate word constructed from the word search grid
        char word[WORDLENGTH];

        // Reset the word to null values
        memset(word, 0, sizeof(word));

        // Set first character of the candidate word from the grid
        word[0] = grid[i][j];

        int m;
        for (m = 1; m < WORDLENGTH; m++) {
            // Determine the next possible position
            int nextX = currentX + DX[k];
            int nextY = currentY + DY[k];

            // If in bounds, perform binary search for word in the dictionary
            if (inBounds(nextX, nextY, x, y) == 1) {
                word[m] = grid[nextX][nextY];
                currentX = nextX;
                currentY = nextY;

                if (m >= 3) {
                    int found = binsearch(0, numWords, word, dictionary);
                    if (found == 1) {
                        printf("%s\n", word);
                    }
                }
            }
        }
    }
}

/**
 * Performs a binary search recursively for the candidate word in the dictionary array
 *
 * @param low           The lower boundary for the binary search
 * @param high          The upper boundary for the binary search
 * @param searchVal     The candidate word used in the search
 * @param dictionary    The dictionary used in the binary search
 * @return              1 (true) if the word is found; else 0 (false)
 */
int binsearch(int low, int high, char *searchVal, char **dictionary) {
    if (low > high) return 0;
    int mid = (low + high) / 2;

    if (strcmp(dictionary[mid], searchVal) == 0) {
        return 1;
    }
    else if (strcmp(dictionary[mid], searchVal) > 0) {
        return binsearch(low, mid - 1, searchVal, dictionary);
    }
    else {
        return binsearch(mid + 1, high, searchVal, dictionary);
    }
}
